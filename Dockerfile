FROM eclipse-temurin:17-jre-alpine
RUN addgroup -S ekklesia && adduser -S ekklesia -G ekklesia
USER ekklesia
COPY build/libs/ekklesia-news-0.0.1-SNAPSHOT.jar .
EXPOSE 9007
ENTRYPOINT ["sh","-c","java ${JAVA_OPTS} -jar ekklesia-news-0.0.1-SNAPSHOT.jar"]
