package com.masonry.ekklesia.news.dto;

import com.masonry.ekklesia.news.repository.info.Info;
import com.masonry.ekklesia.news.repository.info.Label;
import org.springframework.stereotype.Component;

import java.util.function.Function;

import static com.masonry.ekklesia.news.utils.DateUtils.format;

@Component
public class InfoMapper implements Function<Info,InfoData> {

    @Override
    public InfoData apply(Info info) {
        return new InfoData(info.getId()
                , info.getContent()
                , info.getLabels().stream().map(Label::getName).toList()
                , format(info.getCreatedAt()),info.getLocation());
    }
}
