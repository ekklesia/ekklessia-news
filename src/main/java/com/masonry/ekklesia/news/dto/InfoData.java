package com.masonry.ekklesia.news.dto;

import java.util.List;

public record InfoData(String id, String content, List<String>labels
        ,String createdAt,String location) {
}
