package com.masonry.ekklesia.news.controller;

import com.masonry.ekklesia.news.constant.Keys;
import com.masonry.ekklesia.news.dto.InfoData;
import com.masonry.ekklesia.news.exception.NoDataFoundException;
import com.masonry.ekklesia.news.repository.info.InfoService;
import com.masonry.ekklesia.news.utils.PageBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.masonry.ekklesia.news.constant.Paths.INFOS;
@RestController
public class InfoController {

    private final InfoService infoService;

    public InfoController(InfoService infoService) {
        this.infoService = infoService;
    }

    @GetMapping(path = INFOS, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<InfoData>>getAll(@RequestParam(defaultValue = "10") int size
            , @RequestParam(defaultValue = "0") int page){
        Pageable infoPage = PageBuilder.build(page, size);
        Page<InfoData> data= infoService.findAll(infoPage);

        if(!data.hasContent())
           return ResponseEntity.noContent().build();
        return ResponseEntity.ok()
                .header(Keys.H_PAGE, String.valueOf(data.getNumber()))
                .header(Keys.H_TOTAL_ELEMENTS,String.valueOf( data.getTotalElements()))
                .header(Keys.H_TOTAL_PAGES, String.valueOf(data.getTotalPages()))
                .header(Keys.H_HAS_NEXT_PAGE,String.valueOf(data.hasNext()))
                .body(data.getContent());

    }

}
