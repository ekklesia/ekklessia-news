package com.masonry.ekklesia.news.controller;

public record ResponseError(String path,String message,String timestamp,int status) {
}
