package com.masonry.ekklesia.news.controller;

import com.masonry.ekklesia.news.exception.NoDataFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ControllerAdvisor {


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoDataFoundException.class)
    public ResponseError handleNoDataFound(NoDataFoundException ex, WebRequest request){
        HttpServletRequest req = ((ServletWebRequest)request).getRequest();

        return new ResponseError(req.getRequestURI(),ex.getMessage(),"",HttpStatus.NOT_FOUND.value());
    }
}
