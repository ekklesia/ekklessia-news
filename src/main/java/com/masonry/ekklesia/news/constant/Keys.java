package com.masonry.ekklesia.news.constant;

public final class  Keys {
    public static final String H_PAGE = "x-page";
    public static final String H_TOTAL_ELEMENTS = "x-all";
    public static final String H_TOTAL_PAGES = "x-pages";
    public  static final String H_HAS_NEXT_PAGE = "x-has-next-page";
}
