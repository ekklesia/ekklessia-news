package com.masonry.ekklesia.news.utils;

import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class DateUtils {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm");

    public static String format(TemporalAccessor datetime){
        return DATE_TIME_FORMATTER.format(datetime);
    }


}
