package com.masonry.ekklesia.news.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class PageBuilder {
    private static final int DEFAULT_SIZE=10;
    private static final int MAX_SIZE=50;
    private static final int DEFAULT_PAGE=0;
    public static Pageable build(int page, int size) {
        if(page<0)page=DEFAULT_PAGE;
        if(size<=0)
            size =DEFAULT_SIZE;
        else if (size>MAX_SIZE) {
            size=MAX_SIZE;
        }
        return PageRequest.of(page,size);
    }
}
