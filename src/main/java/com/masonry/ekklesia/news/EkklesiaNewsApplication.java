package com.masonry.ekklesia.news;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EkklesiaNewsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EkklesiaNewsApplication.class, args);
	}

}
