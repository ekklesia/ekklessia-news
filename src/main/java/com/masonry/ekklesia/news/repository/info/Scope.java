package com.masonry.ekklesia.news.repository.info;

public enum Scope {
    COMMUNITY, CHURCH
}
