package com.masonry.ekklesia.news.repository.info;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@Document
public class Label {
    @Id
    private String id;
    private String name;

    public Label(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
