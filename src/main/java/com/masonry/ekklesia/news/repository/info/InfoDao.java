package com.masonry.ekklesia.news.repository.info;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class InfoDao implements DataAccessObject<Info>{

    private final MongoTemplate mongoTemplate;

    public InfoDao(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Page<Info> findAll(Pageable page) {
        Query query = new Query();
        List<Info>infoList = mongoTemplate.find(query.with(page), Info.class);
        return PageableExecutionUtils.getPage(infoList,page,
                () -> mongoTemplate.count(new Query(), Info.class));

    }

    @Override
    public Info save(Info info) {
        return mongoTemplate.save(info);
    }
}
