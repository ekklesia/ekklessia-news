package com.masonry.ekklesia.news.repository.info;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Document
public class Info {
    @Id
    private String id;
    private String content;
    private List<Label>labels = new ArrayList<>();
    private LocalDateTime createdAt = LocalDateTime.now();
    private String createdBy;
    private String location;
    private Scope scope = Scope.CHURCH;

    public Info(String id, String content, List<Label> labels, String createdBy, String location) {
        this.id = id;
        this.content = content;
        this.labels = labels;
        this.createdBy = createdBy;
        this.location = location;
    }

    public Info(String content, List<Label> labels, String createdBy, String location) {
        this.content = content;
        this.labels = labels;
        this.createdBy = createdBy;
        this.location = location;
    }
}
