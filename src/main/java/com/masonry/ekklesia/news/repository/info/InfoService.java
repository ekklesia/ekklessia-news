package com.masonry.ekklesia.news.repository.info;

import com.masonry.ekklesia.news.dto.InfoData;
import com.masonry.ekklesia.news.dto.InfoMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InfoService {

    private final DataAccessObject<Info> infoDao;
    private final InfoMapper infoMapper;

    public InfoService(DataAccessObject<Info> infoDao, InfoMapper infoMapper) {
        this.infoDao = infoDao;
        this.infoMapper = infoMapper;
    }

    public Page<InfoData> findAll(Pageable infoPage) {
            return infoDao.findAll(infoPage)
                    .map(infoMapper);
    }
}
