package com.masonry.ekklesia.news.repository.info;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DataAccessObject<T> {
     Page<T> findAll(Pageable page);

     T save(T t);
}
