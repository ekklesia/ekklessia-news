package com.masonry.ekklesia.news.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;


public class MongoDb {
    private final Logger logger = LogManager.getLogger(getClass());
    @Value("mongo.db.uri")
    private String uri;

    @Value("mongo.db.name")
    private String dbName;
   // @Bean
    public MongoClient mongo(){
        logger.info("********************* CONNECTION STRING : {} ##*",uri);
        logger.info("********************* DB NAME : {} ##*",dbName);
       final ConnectionString connectionString = new ConnectionString(uri);
      final  MongoClientSettings mongoClientSettings= MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();
        return MongoClients.create(mongoClientSettings);
    }

   // @Bean
    public MongoTemplate mongoTemplate()throws Exception{
        return new MongoTemplate(mongo(),dbName);
    }
}
