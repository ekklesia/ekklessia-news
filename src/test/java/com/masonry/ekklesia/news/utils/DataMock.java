package com.masonry.ekklesia.news.utils;

import com.masonry.ekklesia.news.dto.InfoData;
import com.masonry.ekklesia.news.dto.InfoMapper;
import com.masonry.ekklesia.news.repository.info.Info;
import com.masonry.ekklesia.news.repository.info.Label;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;

import java.util.ArrayList;
import java.util.List;

public class DataMock {

    public static Page<Info> listOfInfo(int count, Pageable page) {
        List<Info> listOfInfo = new ArrayList<>();
        for(int i=0;i<count;i++)
            listOfInfo.add(new Info("a14e2f6f655f2078ac"+i,"content "+i
                    ,List.of(new Label("e145a364f478b145f","label 1"), new Label("e145a314f477b145f", "label 2"))
                    ,"Admin","Isräel akouedo"));
        return PageableExecutionUtils.getPage(listOfInfo,page, listOfInfo::size);
    }

    public static Page<InfoData>listOfInfoData(int count,Pageable page){
        List<InfoData>data = listOfInfo(count,page).stream().map(new InfoMapper()).toList();
        return PageableExecutionUtils.getPage(data,page,data::size);
    }
}
