package com.masonry.ekklesia.news.repository.info;

import com.masonry.ekklesia.news.dto.InfoData;
import com.masonry.ekklesia.news.dto.InfoMapper;
import com.masonry.ekklesia.news.repository.info.Info;
import com.masonry.ekklesia.news.repository.info.InfoDao;
import com.masonry.ekklesia.news.repository.info.InfoService;
import com.masonry.ekklesia.news.repository.info.Label;
import com.masonry.ekklesia.news.utils.DataMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class InfoServiceTest {
    @Mock
    private InfoDao infoDao;

    @Autowired
    InfoMapper infoMapper;

    private InfoService infoService;

    @BeforeEach
    public void setUp(){
        infoService = new InfoService(infoDao,new InfoMapper());
    }

    @Test
    public void testInfoFound(){
        Pageable page = PageRequest.of(1,10);
        BDDMockito.given(infoDao.findAll(page))
                .willReturn(DataMock.listOfInfo(page.getPageSize(),page));

        Page<InfoData>infoData = infoService.findAll(page);

        assertNotNull(infoData);
        assertEquals(10,infoData.getContent().size());
        assertEquals(10, infoData.getSize());
        assertEquals(1, infoData.getNumber());

    }

    @Test
    public void testDataNotFountOnFindAll(){
        Pageable page = PageRequest.of(1,10);
        BDDMockito.given(infoDao.findAll(page))
                .willReturn(DataMock.listOfInfo(0,page));
        Page<InfoData>infoData = infoService.findAll(page);

        assertNotNull(infoData);
        assertFalse(infoData.hasContent());




    }


}
