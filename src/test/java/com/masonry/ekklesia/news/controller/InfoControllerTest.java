package com.masonry.ekklesia.news.controller;

import com.masonry.ekklesia.news.constant.Keys;
import com.masonry.ekklesia.news.constant.Paths;
import com.masonry.ekklesia.news.dto.InfoData;
import com.masonry.ekklesia.news.dto.InfoMapper;
import com.masonry.ekklesia.news.repository.info.DataAccessObject;
import com.masonry.ekklesia.news.repository.info.Info;
import com.masonry.ekklesia.news.repository.info.InfoService;
import com.masonry.ekklesia.news.repository.info.Label;
import com.masonry.ekklesia.news.utils.DataMock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.headers.HeaderDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.RequestDocumentation;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static com.masonry.ekklesia.news.constant.Keys.*;
import static org.springframework.data.support.PageableExecutionUtils.getPage;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@WebMvcTest({InfoController.class, InfoMapper.class})
public class InfoControllerTest {

    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @MockBean
    private InfoService infoService;

    @MockBean
    DataAccessObject<Info> infoDao;

    @BeforeEach
    public void setUp(RestDocumentationContextProvider provider){
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(MockMvcRestDocumentation.documentationConfiguration(provider))
                .build();

    }


    @Test
    public void testNoContentOnGetAll() throws Exception{
        List<InfoData>emptyList = new ArrayList<>();
        Pageable page = PageRequest.of(0,10);
        BDDMockito.given(infoService.findAll(page)).willReturn(getPage(emptyList,page,()-> 0));
        mockMvc.perform(MockMvcRequestBuilders.get(Paths.INFOS)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent());

    }

    @Test
    public void testFirstPageFoundOnGetAll() throws Exception{
        //Logger logger = LogManager.getLogger(getClass());
        Pageable page = PageRequest.of(0,2);
        Page<InfoData> data = DataMock.listOfInfoData(12,page);
        BDDMockito.given(infoService.findAll(page)).willReturn(data);
        mockMvc.perform(MockMvcRequestBuilders.get(Paths.INFOS)
                        .param("page","0")
                        .param("size", "2")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(header().string(H_PAGE,"0"))
                .andExpect(header().string(H_HAS_NEXT_PAGE, "true"))
                .andExpect(header().string(H_TOTAL_PAGES,"6"))
                .andExpect(header().string(H_TOTAL_ELEMENTS, "12"))
                .andDo(document("get-info",RequestDocumentation.requestParameters(
                        RequestDocumentation.parameterWithName("page").description("Page number. The default value is 0.")
                        ,RequestDocumentation.parameterWithName("size").description("Number of element. The default value is 10 and the max value is 50")
                )))
                .andDo(document("get-info",HeaderDocumentation.responseHeaders(
                        headerWithName(H_PAGE).description("current page")
                ,headerWithName(H_HAS_NEXT_PAGE).description("Indicate the existence of a next page or not")
                ,headerWithName(H_TOTAL_PAGES).description("The total amount of pages")
                ,headerWithName(H_TOTAL_ELEMENTS).description("the total amount of elements"))))
                .andDo(document("get-info", PayloadDocumentation.responseFields(
                        fieldWithPath("[].id").description("Id of info")
                        ,fieldWithPath("[].content").description("Info content")
                        ,fieldWithPath("[].labels").description("List Of labels")
                        ,fieldWithPath("[].createdAt").description("Creation date")
                        ,fieldWithPath("[].location").description("Church's name where the info is from")
                )));

    }

//    @Test
//    public void testFoundLastPageOnGetAll() throws Exception{
//        Pageable page = PageRequest.of(2,5);
//        Page<InfoData> data = DataMock.listOfInfoData(12,page);
//        BDDMockito.given(infoService.findAll(page)).willReturn(data);
//        mockMvc.perform(MockMvcRequestBuilders.get(Paths.INFOS)
//                        .param("page","2")
//                        .param("size", "5")
//                        .accept(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(status().isOk())
//                .andExpect(header().string(H_PAGE,"2"))
//                .andExpect(header().string(H_TOTAL_PAGES,"3"))
//                .andExpect(header().string(H_HAS_NEXT_PAGE, "false"))
//                .andExpect(header().string(H_TOTAL_ELEMENTS, "12"));
//
//    }
//




}
