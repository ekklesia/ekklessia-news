package com.masonry.ekklesia.news.repository.info;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class InfoDaoIntTest {
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    private  InfoDao infoDao;

    private  static MongoDBContainer container = new MongoDBContainer("mongo:5.0").withExposedPorts(27017);

    @DynamicPropertySource
    public static void properties(DynamicPropertyRegistry registry){
        container.start();
        registry.add("spring.data.mongodb.uri",container::getReplicaSetUrl);
    }




    @AfterEach
    public void cleanData(){
        mongoTemplate.findAllAndRemove(new Query(),Info.class);
    }

    @Test
    public void testNoDataFoundOnFindAll(){
        Page<Info>emptyInfo = infoDao.findAll(PageRequest.of(1,10));
        assertNotNull(emptyInfo);
        assertFalse(emptyInfo.hasContent());

    }

    @Test
    public void testGetFirstPage(){
        mongoTemplate.insertAll(listOfInfo(20));

        Page<Info>infoPage = infoDao.findAll(PageRequest.of(0,10));

        assertNotNull(infoPage);
        assertEquals(20,infoPage.getTotalElements());
        assertEquals(10, infoPage.getSize());
        assertEquals(2, infoPage.getTotalPages());
        assertEquals(0, infoPage.getNumber());
    }

    @Test
    public void testGetThirdPageOfAllInfo(){
        mongoTemplate.insertAll(listOfInfo(40));

        Page<Info>infoPage = infoDao.findAll(PageRequest.of(2,10));

        assertNotNull(infoPage);
        assertEquals(40,infoPage.getTotalElements());
        assertEquals(10, infoPage.getSize());
        assertEquals(4, infoPage.getTotalPages());
        assertEquals(2, infoPage.getNumber());
        assertFalse(infoPage.isLast());

    }

    @Test
    public void testGetLastPageOfAllInfo(){
        mongoTemplate.insertAll(listOfInfo(42));

        Page<Info>infoPage = infoDao.findAll(PageRequest.of(4,10));

        assertNotNull(infoPage);
        assertEquals(42,infoPage.getTotalElements());
        assertEquals(10, infoPage.getSize());
        assertEquals(5, infoPage.getTotalPages());
        assertEquals(4, infoPage.getNumber());
        assertTrue(infoPage.isLast());
    }

    private List<Info> listOfInfo(int count) {
        List<Info>listOfInfo = new ArrayList<>();
        for(int i=0;i<count;i++)
            listOfInfo.add(new Info("content "+i
                    ,List.of(new Label("e145a364f478b145f","label 1"), new Label("e145a314f477b145f", "label 2"))
                    ,"Admin","Isräel akouedo"));
        return listOfInfo;
    }

}
