dbAdmin = db.getSiblingDB("admin");

dbAdmin.auth({
  user: "paul",
  pwd: "proct0r4_",
  mechanisms: ["SCRAM-SHA-1"],
  digestPassword: true,
});


// Create DB and collection
db = new Mongo().getDB("ekklesia-news");
db.createUser({
  user: 'ekklesia',
  pwd: 'ek075Qhp',
  roles: [
    {
      role: 'readWrite',
      db: 'ekklesia-news',
    },
  ],
});


db.createCollection("test", { capped: false });
